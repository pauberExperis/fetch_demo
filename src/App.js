import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';

require('dotenv').config();

class App extends React.Component {
    state = {
      isLoading: true,
      userInfo: [],
      error: null,
      counter: 0,
    };


  fetchInfo() {
    fetch(`https://gitlab.com/api/v4/user`,
    {
      headers: {
        'Content-Type': 'application/json',
        'Private-Token': process.env.REACT_APP_API_TOKEN
      }
    })
  .then(response => response.json()).then(data => {this.setState({userInfo: data, isLoading: false});}).catch(error => this.setState({ error, isLoading: false}));
  }
  render() {
      const { isLoading, userInfo, error } = this.state;
      let { counter } = this.state;
      return (
        <React.Fragment>
          <h1>Gitlab info for pauberExperis</h1>

          {error ? <p>{error.message}</p> : null}

          {!isLoading ? (
            Object.keys(userInfo).map(() => {
              const username = userInfo.username;
              const name = userInfo.name;
              const avatar_url = userInfo.avatar_url;
              if (counter == 0) {
                counter++;
                return (
                <div key={username}>
                  <p>Username: {username}</p>
                  <p>Name: {name}</p>
                  <p>Avatar: <img src={avatar_url}/></p>
                </div>
              );
              }
            })
          ) : (
            <h3>Loading...</h3>
          )}
        </React.Fragment>
      )  
  }
  componentDidMount() {
    this.fetchInfo();
  }
 
}

export default App;
